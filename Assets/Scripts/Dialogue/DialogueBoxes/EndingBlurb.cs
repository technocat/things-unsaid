﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EndingBlurb : MonoBehaviour
{
  private string targetDialogue;
  private int endingMode;
  private Text text;
  public SceneTransition transitionMgr;

  void Awake()
  {
    text=GetComponent<Text>();
  }
    // Start is called before the first frame update
    void Start()
    {
      if(targetDialogue.Contains("Nothing left unsaid.")){
        GameObject.FindGameObjectWithTag("BackgroundMusic").GetComponent<MusicScript>().StartCoroutine("FadeOut");
        GameObject.FindGameObjectWithTag("HappyMusic").GetComponent<MusicScript>().StartCoroutine("FadeIn");
      }else if(targetDialogue.Contains("Too many things left unsaid")){
        GameObject.FindGameObjectWithTag("BackgroundMusic").GetComponent<MusicScript>().StartCoroutine("FadeOut");
        GameObject.FindGameObjectWithTag("NeutralMusic").GetComponent<MusicScript>().StartCoroutine("FadeIn");
      }else if (targetDialogue.Contains("The thread has snapped")){
        GameObject.FindGameObjectWithTag("BackgroundMusic").GetComponent<MusicScript>().StartCoroutine("FadeOut");
        GameObject.FindGameObjectWithTag("SadMusic").GetComponent<MusicScript>().StartCoroutine("FadeIn");
      }
      updateText();
        if(endingMode==0){
          //soft
          transitionMgr.endingMode=true;
        }else if(endingMode==1){
          //hard
          transitionMgr.endingMode=false;
        }
    }

    void OnEnable()
    {
      targetDialogue=PlayerPrefs.GetString("dialogue");
      endingMode=PlayerPrefs.GetInt("end");
      Debug.Log("ENDING BLURB LOADING DIALOGUE:"+targetDialogue);
      Debug.Log("ENDING BLURB LOADING ENDING:"+endingMode);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void updateText(){
      //do animation?
      text.text=targetDialogue;
    }
}
