﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InternalSelfDialogueScript : MonoBehaviour
{
   public float delay = 0.1f;
    private string currentText = "";

    public bool textVisible = false;
    public bool isVisible = false;

    private Image image;

    private Text textbox;
    private string dName="INTERNAL_DIALOGUE: ";

  AudioSource audioData;
    void Awake()
    {
        image = gameObject.GetComponent<Image>();
        textbox = gameObject.GetComponentInChildren<Text>();
        audioData = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*public void setText(string targetText,float delay){
        Debug.Log(dName+"SET TEXT");
        textVisible=false;
        StartCoroutine(showText(targetText,delay));
    }*/

    public IEnumerator setVisible(float delay){
        while(image.color.a<=1f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a+.02f);   
            yield return new WaitForSecondsRealtime(delay);
            //Debug.Log(dName+"image colour = "+image.color.a);
        }
        if(image.color.a!=1f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,1f);
        }
        isVisible=true;
    }

    public IEnumerator setInvisible(float delay){
        removeText();
        while(image.color.a>=0f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a-.02f);
            yield return new WaitForSecondsRealtime(delay);
        }
        if(image.color.a!=0f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,0f);
        }
        isVisible=false;
        
    }

    public void removeText(){
        Debug.Log(dName+"REMOVE TEXT");
        textbox.text = "";
        textVisible=false;
    }

    public IEnumerator setText(string targetText,float delay){
      audioData.Play();
        Debug.Log(dName+"SHOW TEXT:"+targetText);
        for(int i = 0; i <= targetText.Length; i++){
            currentText = targetText.Substring(0,i);
            textbox.text = currentText;
            yield return new WaitForSecondsRealtime(delay);
        }
        textVisible=true;
        Debug.Log(dName+"TEXT IS NOW:"+textVisible);
        audioData.Stop();
    }
}

