﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AsheDialogueScript : MonoBehaviour
{
    public float delay = 0.1f;
    private string currentText = "";

    public bool textVisible = false;

    private Text textbox;
    private string dName="ASHE_DIALOGUE: ";
    AudioSource audioData;

    void Awake()
    {
        textbox = gameObject.GetComponent(typeof(Text)) as Text;
        audioData = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        textVisible=false;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*public void setText(string targetText,float delay){
        Debug.Log(dName+"SET TEXT");
        textVisible=false;
        StartCoroutine(showText(targetText,delay));
    }*/

    public void removeText(){
        Debug.Log(dName+"REMOVE TEXT");
        textbox.text = " ";
        textVisible=false;
    }

    public IEnumerator setText(string targetText,float delay){
      audioData.Play();
        Debug.Log(dName+"SHOW TEXT:"+targetText);
        for(int i = 0; i <= targetText.Length; i++){
            currentText = targetText.Substring(0,i);
            textbox.text = currentText;
            yield return new WaitForSecondsRealtime(delay);
        }
        textVisible=true;
        Debug.Log(dName+"TEXT IS NOW:"+textVisible);
        audioData.Stop();
    }
}
