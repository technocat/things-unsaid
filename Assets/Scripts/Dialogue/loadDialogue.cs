﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using UnityEngine.Events;



public class loadDialogue : MonoBehaviour
{
    public TextAsset dialogueFile;
    public List<int> entryID = new List<int>{};//: 1
    public List<string> speaker=new List<string>{};//: "Friend"
    public List<int> backgroundImage=new List<int>{};//:0
    public List<string> AsheDialogueText=new List<string>{};//:"Hello~"
    public List<string> RaniDialogueText=new List<string>{};//:"Hello~"
    public List<string> optionText1=new List<string>{};//:"uhhhhh"
    public List<string> optionText2=new List<string>{};//:"I Have No Clue what to Say"
    public List<int> optionLink1=new List<int>{};//:1
    public List<int> optionLink2=new List<int>{};//:2
    public List<string> innerDialogue=new List<string>{};//:"uhhhhh"
    public List<string> innerSpeaker=new List<string>{};//:"I Have No Clue what to Say"
    private string dName="LOAD_DIALOGUE:";
    void Awake()
    {
        loadDialogueFromFile();
        //fetchDialogue();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Iterate through the String Array line by line, adding that information into an dialogueHolder
    private void lineParser(string[] sA){
        switch (sA[0]){
            case "entry"://: 1
                entryID.Add(stringToInt(sA[1]));
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "who"://: "Friend"
                speaker.Add(sA[1]);
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "background"://:0
                backgroundImage.Add(stringToInt(sA[1]));
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "AsheDialogue":
                AsheDialogueText.Add(sA[1]);
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "RaniDialogue"://:"Hello~"
                RaniDialogueText.Add(sA[1]);
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "optionText_1"://:"uhhhhh"
                optionText1.Add(sA[1]);
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "optionText_2"://:"I Have No Clue what to Say"
                optionText2.Add(sA[1]);
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "optionLink_1"://:1
                optionLink1.Add(stringToInt(sA[1]));
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "optionLink_2"://:2
                optionLink2.Add(stringToInt(sA[1]));
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "innerSpeaker":
                innerSpeaker.Add(sA[1]);
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            case "innerDialogue":
                innerDialogue.Add(sA[1]);
                Debug.Log(dName+sA[0]+":"+sA[1]);
                break;
            default:
                Debug.LogError(dName+"INVALID TEXT ENTRY");
                Debug.LogError(sA[0]+":"+sA[1]);
                break;
        }
    }

    //Helper to convert a string to an int with exception catching
    private int stringToInt(string s){
        int i = 0;
        try{
            i = Int16.Parse(s);
        } catch (Exception e){ Debug.LogError(e);}
        return i;
    }

    private void loadDialogueFromFile(){
        try{
            using (StringReader sr = new StringReader(dialogueFile.text))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                { 
                    string[] temp = line.Split(':');               
                    lineParser(temp);
                }
            }
        }catch (Exception e)
        {
            Debug.LogError(dName+"File could not be read:");
            Debug.LogError(e.Message);
        }
        
    }

    //Load the Dialogue from file
    private void fetchDialogue(){
        //Load the dialogue
        try
        {
            /*Create instance of StreamReader to read from file, also close it
            using (StreamReader sr = new StreamReader(dialoguePath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                { 
                    string[] temp = line.Split(':');               
                    lineParser(temp);
                }
            }*/
        }
        catch (Exception e)
        {
            Debug.LogError(dName+"File could not be read:");
            Debug.LogError(e.Message);
        }
    }

}
