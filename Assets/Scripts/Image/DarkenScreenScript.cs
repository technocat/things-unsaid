﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DarkenScreenScript : MonoBehaviour
{

    private Image image;

    private string dName="DARKEN_SCREEN: ";
    public bool isVisible=false;

    public float maxOpacity=0.4f;

    void Awake()
    {
        image = gameObject.GetComponent<Image>();
    }
    // Start is called before the first frame update
    void Start()
    {
        if(image.color.a==0f){
            isVisible=false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setVisibility(bool visibility,float delay){
        if (!visibility){
            Debug.Log(dName+"Set to: "+visibility);
            StartCoroutine(setInvisible(delay));
        } else if (visibility){
            Debug.Log(dName+"Set to: "+visibility);
            StartCoroutine(setVisible(delay));
        } else{
            Debug.Log(dName+"Wrong visibility Setting --- How????");
        }
    }

    public IEnumerator setVisible(float delay){
        Debug.Log(dName+"IN COROUTINE SET VISIBLE");
        for(int i = 0; i < 100; i++){
            if(image.color.a<maxOpacity){
                image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a+.01f);
            }else if(image.color.a>maxOpacity){
                yield break;
            }
            
            yield return new WaitForSeconds(delay);
        }
        isVisible=true;
    }

    IEnumerator setInvisible(float delay){
        Debug.Log(dName+"IN COROUTINE 1 SET INVISIBLE");
        for(int i = 0; i < 100; i++){
            if(image.color.a>0f){
                image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a-.01f);
            }

            yield return new WaitForSeconds(delay);
        }
        if(image.color.a<0f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,0f);
        }
        isVisible=false;

    }
}
