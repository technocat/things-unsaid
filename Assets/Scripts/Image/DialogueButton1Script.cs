﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueButton1Script : MonoBehaviour
{
    private Image image;

    private Text buttonText;

    //private Color colour;

    public bool textVisible=false;
    private string currentText;
    private Button button;
    private string dName="BUTTON_1: ";

    public bool isVisible=true;
    private bool running;
    AudioSource audioData;

    void Awake()
    {
        image = gameObject.GetComponent<Image>();
        buttonText = gameObject.GetComponentInChildren<Text>();
        button = gameObject.GetComponent<Button>();
        audioData = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
      textVisible=false;
        //running=true;
        //StartCoroutine(checkButtonVis());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator setVisible(float imageDelay, float textDelay, string targetText){
      buttonText.color= new Color(0f,0f,0f,0f);
      yield return StartCoroutine(showText(targetText,textDelay));
        while(!(image.color.a>=1f)){
            image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a+.01f);  
            buttonText.color = new Color(0f,0f,0f,buttonText.color.a+.01f); 
            yield return new WaitForSecondsRealtime(imageDelay);
            //Debug.Log(dName+"image colour = "+image.color.a);
        }
        if(image.color.a!=1f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,1f);
            buttonText.color = new Color(buttonText.color.r,buttonText.color.g,buttonText.color.b,1f); 
        }
        //audioData.Play();
        
        //audioData.Stop();
        isVisible=true;
    }

    public IEnumerator setInvisible(float delay){
      if(!(delay==0f)){
        while(!(image.color.a<=0f)){
            image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a-.01f);
            yield return new WaitForSecondsRealtime(delay);
        }
        if(image.color.a!=0f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,0f);
        }
      }else{
        setInvisibleImmediate();
      }
        hideButtonText();
        isVisible=false;
    }

    public void setInvisibleImmediate(){
        image.color = new Color(image.color.r,image.color.g,image.color.b,0f);
        hideButtonText();
        isVisible=false;
    }

    public IEnumerator showText(string targetText,float delay){
        Debug.Log(dName+"IN COROUTINE SHOW TEXT");
        Debug.Log(dName+"textVisible="+textVisible);
        buttonText.text = targetText;
        
        // for(int i = 0; i <= targetText.Length; i++){
        //     currentText = targetText.Substring(0,i);
            
        //     //Debug.Log(dName+"RENDERING TEXT: "+currentText);
        //     yield return new WaitForSecondsRealtime(delay);
        // }
        Debug.Log(dName+"textVisible="+textVisible);
        textVisible=true;
        yield return null;
    }
    public void setInteractable(bool interact){
        button.interactable = interact;
    }

    public bool getInteractable(){
        return button.interactable;
    }
    private void hideButtonText(){
      buttonText.color= new Color(buttonText.color.r,buttonText.color.g,buttonText.color.b,0f);
        buttonText.text=" ";
        textVisible=false;
    }

}