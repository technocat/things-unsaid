﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThinkingBubbleScript : MonoBehaviour
{
    private Image bubbleImage;
    public float imageDelay = .001f;
    public float pauseDelay = 0.1f;

    public Sprite bubble1;
    public Sprite bubble2;
    public Sprite bubble3;
    public bool bubbleVisible;
    private string dName="IMAGE_THINK:";

    private float alpha=0f;
    public int wait=20;
    private Color InvisWhite = new Color(1,1,1,0);

    void Awake()
    {
        bubbleImage = gameObject.GetComponent<Image>();
    }
    // Start is called before the first frame update
    void Start()
    {
        bubbleVisible=false;
        bubbleImage.color = InvisWhite;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setVisibility(bool visibility){
        Debug.Log(dName+"SET VISIBLE "+visibility);
        if(visibility){
            bubbleVisible=true;
            StartCoroutine(setAnimated(imageDelay,wait));
        }else{
            bubbleVisible=false;
            setInvisibleImmediate();
        }
    }

    IEnumerator setAnimated(float delay,int wait){
        Debug.Log(dName+"IN COROUTINE SET ANIMATED");
        for(int i = 0; i < wait; i++){
            //Debug.Log(dName+"IN COROUTINE: "+i);
            if(bubbleImage.color.a<1f){
                //Debug.Log(dName+"WAIT: "+wait);
                alpha=(float)i/wait;
                //Debug.Log(dName+"ALPHA: "+alpha);
                bubbleImage.color = new Color(bubbleImage.color.r,bubbleImage.color.g,bubbleImage.color.b,alpha);
            }
            yield return new WaitForSeconds(delay);
        }
        while(bubbleVisible){
            bubbleImage.sprite = bubble2;
            yield return new WaitForSeconds(pauseDelay);
            
            bubbleImage.sprite = bubble3;
            yield return new WaitForSeconds(pauseDelay);
            
            bubbleImage.sprite = bubble1;
            yield return new WaitForSeconds(pauseDelay);
            
        }
    }

    IEnumerator setInvisible(float delay){
        Debug.Log(dName+"IN COROUTINE SET INVISIBLE");
        for(int i = 0; i < 100; i++){
            if(bubbleImage.color.a>0f){
                bubbleImage.color = new Color(bubbleImage.color.r,bubbleImage.color.g,bubbleImage.color.b,(bubbleImage.color.a-.01f));
            }
            yield return new WaitForSeconds(delay);
        }
    }

    private void setInvisibleImmediate(){
        //while(!finishedAnimation){
        //    Debug.LogError("THINKING BUBBLE: Uh, invisibility fail");
        //}
        bubbleImage.color = new Color(bubbleImage.color.r,bubbleImage.color.g,bubbleImage.color.b,0f);
    }











    /*
    IEnumerator setOldAnimated(float delay,float pauseDelay){
        while(bubbleVisible){
            thinkingImage.sprite = bubble1;
            for(int i = 0; i < 100; i++){
                //Debug.Log("Thinking: IN COROUTINE: "+i);
                colour = thinkingImage.color;
                if(thinkingImage.color.a<1f){
                    thinkingImage.color = new Color(thinkingImage.color.r,thinkingImage.color.g,thinkingImage.color.b,thinkingImage.color.a+.01f);
                }
                yield return new WaitForSeconds(delay);
            }
            for (int i=0; i<1; i++){
                yield return new WaitForSeconds(pauseDelay);
            }
            for(int i = 0; i < 100; i++){
                //Debug.Log("Thinking: IN COROUTINE: "+i);
                colour = thinkingImage.color;
                if(thinkingImage.color.a>0f){
                    thinkingImage.color = new Color(thinkingImage.color.r,thinkingImage.color.g,thinkingImage.color.b,thinkingImage.color.a-.01f);
                }
                yield return new WaitForSeconds(delay);
            }
            thinkingImage.sprite = bubble2;
            for(int i = 0; i < 100; i++){
                //Debug.Log("Thinking: IN COROUTINE: "+i);
                colour = thinkingImage.color;
                if(thinkingImage.color.a<1f){
                    thinkingImage.color = new Color(thinkingImage.color.r,thinkingImage.color.g,thinkingImage.color.b,thinkingImage.color.a+.01f);
                }
                yield return new WaitForSeconds(delay);
            }
            for (int i=0; i<1; i++){
                yield return new WaitForSeconds(pauseDelay);
            }
            for(int i = 0; i < 100; i++){
                //Debug.Log("Thinking: IN COROUTINE: "+i);
                colour = thinkingImage.color;
                if(thinkingImage.color.a>0f){
                    thinkingImage.color = new Color(thinkingImage.color.r,thinkingImage.color.g,thinkingImage.color.b,thinkingImage.color.a-.01f);
                }
                yield return new WaitForSeconds(delay);
            }
            thinkingImage.sprite = bubble3;
            for(int i = 0; i < 100; i++){
                //Debug.Log("Thinking: IN COROUTINE: "+i);
                colour = thinkingImage.color;
                if(thinkingImage.color.a<1f){
                    thinkingImage.color = new Color(thinkingImage.color.r,thinkingImage.color.g,thinkingImage.color.b,thinkingImage.color.a+.01f);
                }
                yield return new WaitForSeconds(delay);
            }
            for (int i=0; i<1; i++){
                yield return new WaitForSeconds(pauseDelay);
            }
            for(int i = 0; i < 100; i++){
                //Debug.Log("Thinking: IN COROUTINE: "+i);
                colour = thinkingImage.color;
                if(thinkingImage.color.a>0f){
                    thinkingImage.color = new Color(thinkingImage.color.r,thinkingImage.color.g,thinkingImage.color.b,thinkingImage.color.a-.01f);
                }
                yield return new WaitForSeconds(delay);
            }
        } 
        
    }*/
}
