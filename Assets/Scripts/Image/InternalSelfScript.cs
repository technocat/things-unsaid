﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InternalSelfScript : MonoBehaviour
{

    /*
    *
    *   THIS IS THE MAIN SCRIPT IT HAS THE DIALOGUE BOX UNDEER IT UGH
    *
    */
    private Image image;

    private Text textBox;

    //private Color colour;

    public InternalSelfDialogueScript internalDialogue;

    private bool textVisible;
    public bool isVisible;
    private string currentText;
    private Button button;
    private string dName="INTERNAL SELF: ";

    public Sprite red;
    public Sprite blue;//a
    public Sprite green;
    public Sprite yellow;


    void Awake()
    {
        textVisible=false;
        image = gameObject.GetComponent<Image>();
        textBox = gameObject.GetComponentInChildren<Text>();
        if(!textVisible){
          Debug.Log("why?");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        isVisible=false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator setVisible(float delay){
        while(!(image.color.a>=1f)){
            image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a+.01f);   
            yield return new WaitForSeconds(delay);
            //Debug.Log(dName+"image colour = "+image.color.a);
        }
        if(image.color.a!=1f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,1f);
        }
        isVisible=true;
    }

    public IEnumerator setInvisible(float delay){
        setText("",0f);
        while(!(image.color.a<=0f)){
            image.color = new Color(image.color.r,image.color.g,image.color.b,image.color.a-.01f);
            yield return new WaitForSeconds(delay);
        }
        if(image.color.a!=0f){
            image.color = new Color(image.color.r,image.color.g,image.color.b,0f);
        }
        isVisible=false;
        
    }

    public void setText(string targetText, float textDelay){
        StartCoroutine(showText(targetText, textDelay));
    }

    IEnumerator showText(string targetText,float delay){
        Debug.Log(dName+"IN COROUTINE SHOW TEXT");
        textVisible=false;
        for(int i = 0; i <= targetText.Length; i++){
            currentText = targetText.Substring(0,i);
            textBox.text = currentText;
            yield return new WaitForSeconds(delay);
        }
        textVisible=true;
    }
    public void setInteractable(bool interact){
        button.interactable = interact;
    }

    private void hideDialogueText(){
        textBox.text="";
    }

    public void setImage(int speaker){
        switch (speaker)
        {
            case 0:
                image.sprite=red;
                break;
            case 1:
                image.sprite=green;
                break;
            case 2:
                image.sprite=blue;
                break;
            case 3:
                image.sprite=yellow;
                break;
            default:
                image.sprite=red;
                break;
        }
    }
}
