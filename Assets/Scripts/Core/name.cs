﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Bolt;


public class name : MonoBehaviour
{
    public TMP_Text textName;
    private List<string> text;
    private int currentEntry = 0;
    private bool loaded=false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentEntry = (int)Variables.Application.Get("currentEntry");
        loadIfNotLoaded();
        updateIfNew();
    }

    //todo make smert
    void loadIfNotLoaded(){
        if (loaded != true){
            text = (List<string>)Variables.Application.Get("speaker");
            loaded=true;
        }
    }

    void updateIfNew(){
        if (textName.text != text[currentEntry]){
            textName.text = text[currentEntry];
        }
    }
}
