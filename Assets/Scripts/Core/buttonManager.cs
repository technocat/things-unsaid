﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonManager : MonoBehaviour
{
    public Button Option1Button, Option2Button;
    public DialogueButton1Script button1;
    public DialogueButton2Script button2;

    public bool buttonsVisible=true;

    public bool buttonOneClicked=false;

    public bool buttonTwoClicked=false;


    void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // if((button1.isVisible==true)&&(button2.isVisible==true)){
        //     buttonsVisible=true;
        // }else if((button1.isVisible==false)&&(button2.isVisible==false)){
        //     buttonsVisible=false;
        // }else{
        //     Debug.Log("Transitioning!");
        // }

    }

    public IEnumerator setButtonVisibility(bool vis,int button,float imageDelay, float textDelay, string targetText1,string targetText2){
      if(vis){
        switch (button)
        {
          case 0:
            StartCoroutine(button1.setVisible(imageDelay,textDelay,targetText1));
            StartCoroutine(button2.setVisible(imageDelay,textDelay,targetText2));
            Debug.Log("BUTTON MANAGER: BUTTON BOTH: VIS:"+vis);
            yield return StartCoroutine(textVisible(button));
            break;
          case 1:
            StartCoroutine(button1.setVisible(imageDelay,textDelay,targetText1));
            Debug.Log("BUTTON MANAGER: BUTTON ONE: VIS:"+vis);
            yield return StartCoroutine(textVisible(button));
            break;
          case 2:
            StartCoroutine(button2.setVisible(imageDelay,textDelay,targetText2));
            Debug.Log("BUTTON MANAGER: BUTTON TWO: VIS:"+vis);
            yield return StartCoroutine(textVisible(button));
            Debug.Log("BUTTON MANAGER: BUTTON TWO: VIS DONE:"+vis);
            break;
          default:
            Debug.Log("BUTTON MANAGER: BUTTON NONE: VIS:"+vis);
          break;
          }
        }else{
          //SET BUTTONS INVISIBLE
          Debug.Log("BUTTON MANAGER: BUTTON BOTH: VIS:"+vis);
          StartCoroutine(button1.setInvisible(imageDelay));
          StartCoroutine(button2.setInvisible(imageDelay));

        }
    
        
        
    }

    //Change the text on either button, writing effect handled by the button script
    public void setButtonText(int buttonNumber, string buttonText, float textDelay){
        if(buttonNumber==0){
            StartCoroutine(button1.showText(buttonText,textDelay));
        }else if(buttonNumber==1){
            StartCoroutine(button2.showText(buttonText,textDelay));
        }else{
            Debug.Log("Wrong Button");
        }
    }

    public void setInteractable(bool interact, int button){
        switch (button)
        {
            case 0:
              button1.setInteractable(interact);
              button2.setInteractable(interact); 
              Debug.Log("BUTTON MANAGER: BUTTON BOTH:"+interact);
            break;
            case 1:
              button1.setInteractable(interact);
              Debug.Log("BUTTON MANAGER: BUTTON ONE:"+interact);
            break;
            case 2:
              button2.setInteractable(interact);
              Debug.Log("BUTTON MANAGER: BUTTON TWO:"+interact); 
            break;
            default:
              Debug.Log("BUTTON MANAGER: BUTTON NONE:"+interact);
            break;
        }
    }

    public bool getInteractable(int buttonIndex){
        if(buttonIndex==0){
            return button1.getInteractable();
        }else if(buttonIndex==1){
            return button2.getInteractable();
        }else{
            return false;
        }
    }

    public IEnumerator textVisible(int buttonIndex){
      switch (buttonIndex)
      {
          case 0:
          while(!((button1.textVisible)&&(button2.textVisible))){
            //Debug.Log("WAITING FOR BOTH BUTTONS");
            yield return null;
          }
          break;
          case 1:
          while(!button1.textVisible){
            //Debug.Log("WAITING FOR BUTTON 1");
            yield return null;
          }
          break;
          case 2:
          while(!button2.textVisible){
            //Debug.Log("WAITING FOR BUTTON 2");
            yield return null;
          }
          break;
          default:
          Debug.LogError("Wrong Button");
          break;
      }

      Debug.Log("ESCAPED TEXTVIS");
        // while(!button1.textVisible){
        //     yield return null;
        // }
        // if(!button2.textVisible){
        //     while(!button2.textVisible){
        //         yield return null;
        //     }
        // }
        // yield return null;
    }
}
