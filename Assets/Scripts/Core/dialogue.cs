﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Bolt;

public class dialogue : MonoBehaviour
{
    public TMP_Text textDialogue;
    private List<string> text;
    private int currentEntry = 0;
    private bool loaded=false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentEntry = (int)Variables.Application.Get("currentEntry");
        loadIfNotLoaded();
        updateIfNew();
    }

    //todo make smert
    void loadIfNotLoaded(){
        if (!loaded){
            text = (List<string>)Variables.Application.Get("dialogueText");
            loaded=true;
        }
    }

    void updateIfNew(){
        if (textDialogue.text != text[currentEntry]){
            textDialogue.text = text[currentEntry];
        }
    }


}
