﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bolt;

public class characterImageHandler : MonoBehaviour
{
    private Image characterImage;
    private GameObject panelCharacter;
    private List<string> speaker;
    private int currentEntry = 0;
    private int oldEntry = 0;
    private bool loaded=false;

    public Sprite Player;
    public Sprite Friend;
    public Sprite Red;

    // Start is called before the first frame update
    void Start()
    {
        panelCharacter = GameObject.Find("PanelMainCharacter");
        characterImage = panelCharacter.GetComponent<Image>();
        Debug.Log(characterImage.sprite.texture.name);
        currentEntry = (int)Variables.Application.Get("currentEntry");
        oldEntry = 9999; //forces update on first run
    }

    // Update is called once per frame
     void Update()
    {
        currentEntry = (int)Variables.Application.Get("currentEntry");
        loadIfNotLoaded();
        updateIfNew();
    }

    void loadIfNotLoaded(){
        if (loaded != true){
            speaker = (List<string>)Variables.Application.Get("speaker");
            loaded=true;
        }
    }

    void updateIfNew(){
        if(oldEntry!=currentEntry){
            switch(speaker[currentEntry]){
            case "Ashe":
                characterImage.sprite = Player;
                Debug.Log("Set Speaker Image to Player!");
                break;
            case "Rani":
                characterImage.sprite = Friend;
                Debug.Log("Set Speaker Image to Friendo!");
                break;
            case "Red":
                characterImage.sprite = Red;
                Debug.Log("Set Speaker Image to Red!");
                break;
            default:
                characterImage.sprite = Player;
                Debug.LogError("Defaulted to Player!");
                break;
            }
            oldEntry = currentEntry;
        }
        
    }
}
