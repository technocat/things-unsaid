﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
public class SceneTransition : MonoBehaviour
{

  public Animator transitionAnim;
  public string sceneName;
  
  public InputControls input;

  public float pause;

  public bool endingMode;

  public bool ending;

  public bool playMusic;
  public string musicTag;

  public string endingScene;
  

  public bool inputImmediate;

  void Awake()
  {
    input= new InputControls();
  }
    // Start is called before the first frame update
    void Start()
    {
        if(playMusic){Debug.Log("PLAYING MUSIC");play();}
    }

    // Update is called once per frame
    void Update()
    {
      
    }

  public void button(){
    if(ending){
      StartCoroutine(TriggerEnding());
    }else{
      if(inputImmediate){
      StartCoroutine(LoadScene());
    }else{
      StartCoroutine(waitForInput());
    }
    }
    
  }

  void OnEnable()
    {
        input.Enable();
    }

    void OnDisable()
    {
        input.Disable();
    }

    IEnumerator LoadScene(){
      transitionAnim.SetTrigger("end");
      yield return new WaitForSecondsRealtime(pause);
      SceneManager.LoadScene(sceneName);
    }

    IEnumerator waitForInput(){
    while(!input.UI.Click.triggered){
      yield return null;
    }
    StartCoroutine(LoadScene());
  }

    IEnumerator TriggerEnding(){
    if(endingMode){
      //soft
      transitionAnim.SetTrigger("end");
      yield return new WaitForSecondsRealtime(pause);
      SceneManager.LoadScene(sceneName);
    }else{
      //hard
      transitionAnim.SetTrigger("end");
      yield return new WaitForSecondsRealtime(pause);
      SceneManager.LoadScene(endingScene);
    }
  }
    

  private void play(){
    if(!(musicTag=="BackgroundMusic")){
      GameObject.FindGameObjectWithTag("BackgroundMusic").GetComponent<MusicScript>().StartCoroutine("FadeOut");
    }else{
      //GameObject.FindGameObjectWithTag("BackgroundMusic").GetComponent<MusicScript>().StopMusic();
    }
    
    GameObject.FindGameObjectWithTag("SadMusic").GetComponent<MusicScript>().StartCoroutine("FadeOut");
    GameObject.FindGameObjectWithTag("NeutralMusic").GetComponent<MusicScript>().StartCoroutine("FadeOut");
    GameObject.FindGameObjectWithTag("HappyMusic").GetComponent<MusicScript>().StartCoroutine("FadeOut");
    
    GameObject.FindGameObjectWithTag(musicTag).GetComponent<MusicScript>().StartCoroutine("FadeIn");
  }


}
