﻿using System.Collections;
using System.Collections.Generic;
 using UnityEngine;
 
 public class MusicScript : MonoBehaviour
 {
     private AudioSource _audioSource;
     private void Awake()
     {
         DontDestroyOnLoad(transform.gameObject);
         _audioSource = GetComponent<AudioSource>();
     }
 
     public void PlayMusic()
     {
         if (_audioSource.isPlaying) return;
         _audioSource.Play();
     }
 
     public void StopMusic()
     {
         _audioSource.Stop();
     }

     public IEnumerator FadeOut(){
       while(_audioSource.volume>0f){
         _audioSource.volume=_audioSource.volume-.1f;
         yield return new WaitForSecondsRealtime(.02f);
       }
       _audioSource.Stop();
     }

     public IEnumerator FadeIn(){
       _audioSource.volume=0f;
       _audioSource.Play();
       while(_audioSource.volume<.8f){
         _audioSource.volume=_audioSource.volume+.1f;
         yield return new WaitForSecondsRealtime(.2f);
       }
     }
 }
