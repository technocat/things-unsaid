﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Bolt;
public class option2 : MonoBehaviour
{
    // Start is called before the first frame update
    public TMP_Text option2Text;
    private List<string> text;
    private int currentEntry = 0;
    private bool loaded=false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentEntry = (int)Variables.Application.Get("currentEntry");
        loadIfNotLoaded();
        updateIfNew();
    }

    //todo make smert
    void loadIfNotLoaded(){
        if (!loaded){
            text = (List<string>)Variables.Application.Get("option2Text");
            loaded=true;
        }
    }

    void updateIfNew(){
        if (option2Text.text != text[currentEntry]){
            option2Text.text = text[currentEntry];
        }
    }
}
