﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class startButton : MonoBehaviour
{
    public Button targetButton;
    public int targetScene;

    AudioSource m_MyAudioSource;
    bool m_ToggleChange;
    bool m_Play;
    //private string dName="BUTTON_START:";
    void Awake()
    {
        m_MyAudioSource = GetComponent<AudioSource>();
        m_MyAudioSource.volume = 0f;
        //m_MyAudioSource.Play();
        m_MyAudioSource.volume = 1f;
    }

    // Start is called before the first frame update
    void Start()
    {
        targetButton.onClick.AddListener(LoadOnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LoadOnClick(){
        m_MyAudioSource.PlayOneShot(m_MyAudioSource.clip);
        StartCoroutine(loadLevel());
    }

    IEnumerator loadLevel(){
        Debug.Log("LOADING LEVEL");
        for(int i = 0; i < 2; i++){
            if(i==1){

                SceneManager.LoadScene(targetScene);
            }
         
        yield return new WaitForSecondsRealtime(.1f);
        }
    }
}
