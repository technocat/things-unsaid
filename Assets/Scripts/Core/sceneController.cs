﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class sceneController : MonoBehaviour
{

    public AsheDialogueScript AsheDialogue;
    public AsheScript AsheImage;
    public RaniDialogueScript RaniDialogue;
    public InternalSelfDialogueScript InternalSelfDialogue;
    public EndingScript endingScript;
    public ThinkingBubbleScript thinkBubble;
    public InputControls input;

    public InternalSelfScript internalSelfImage;
    public DarkenScreenScript darken;

    //public InternalDialogueScript InternalDialogue;
    private loadDialogue loadDialogue;
    private buttonManager buttons;
    private AudioSource mainAudioSource;

    public SceneTransition transScene;
    public float raniTextDelay;// = 0.05f;
    public float asheTextDelay;// = 0.05f;
    public float buttonDelay;// = .0005f;
    public float buttonTextDelay;//=.04f;
    public float[] internalSelfTextDelay = {0.01f,0.02f,0.02f,0.02f};
    public float[] internalSelfTransitionDelay = {0.02f,0.02f,0.02f,0.02f};
    public int previousDialogue=0;
    public int nowDialogue=0;
    public bool internalSelfModeB=false;
    public bool firstPass=true;
    public bool needsUpdated=true;
    public bool asheTextNeedsUpdated=false;
    public bool asheTextPending=false;
    public bool raniTextPending=false;
    public bool internalVisible=false;
    public bool running=false;

    public string pendingAsheText = "";
    public string pendingRaniText = "";
    public string pendingInternalText = "";
    public string dName="SCENECONTROLLER: ";

    private string targetEndDialogue;
    private int targetEnding;

    public static float tickSpeed=.1f;
    //public static float interactiveWaitSeconds=2f;

    public bool hardEndGame=false;
    public bool softEndGame=false;
    private bool buttonTwoHide=false;
    private bool buttonBothHide=false;
    private int buttonPressed;
    private int internalSpeaker;

    public Animator transitionAnim;

    WaitForSecondsRealtime tick = new WaitForSecondsRealtime(tickSpeed);

  /*
  *
  */
    void Awake()
    {
        //load button audio
        mainAudioSource = GetComponent<AudioSource>();
        //set audio to preload
        mainAudioSource.volume = 0f;

        mainAudioSource.volume = 1f;
        //load dialogue script
        loadDialogue = GetComponent<loadDialogue>();
        //load buttons script
        buttons = GetComponent<buttonManager>();
        input= new InputControls();
    }

  /*
  *
  */
    void OnEnable()
    {
        input.Enable();
    }

  /*
  *
  */
    void OnDisable()
    {
      
        PlayerPrefs.SetString("dialogue",targetEndDialogue);
        PlayerPrefs.SetInt("end",targetEnding);
        input.Disable();
    }

  /*
  *
  */
    void Start()
    {
        running=true;
        firstPass=true;
        needsUpdated=true;
        buttonPressed=-1;
        StartCoroutine(updateScene());
    }

    // Update is called once per frame
    void Update()
    {

       
    }

    /*
    *   Update Scene Coroutine, waits for updates, then triggers the necessary screen updates
    */
    IEnumerator updateScene(){
      //WHILE THE THING IS RUNNING
      while(running){
        //Debug.Log(dName+"updateScene");

        //IF NEEDS UPDATED IS TRIGGERED BY A BUTTON PRESS OR FIRST PASS
          if(needsUpdated){
            needsUpdated=false;
            //previousDialogue=nowDialogue;
            Debug.LogError(dName+"BUTTON PRESS METHOD");
            buttonPressMethod(); 
            Debug.LogError(dName+"HIDE THE THINGS");
            hideTheThings();
            Debug.LogError(dName+"CHECK FOR POTATOES");
            checkForPotatos();
            //Do the initial setup things, only ran once
            //else is ran on every other iteration
            if(firstPass){
              ///FIRST PASS
              Debug.LogError(dName+"FIRST PASS METHOD");
              yield return StartCoroutine(firstPassMethod());
            }else{
              Debug.Log(dName+"XPASS START");
              needsUpdated=false;
              
              //IF NOT END? --WHY DID I DO THIS
              if(!(softEndGame||hardEndGame)){
                Debug.LogError(dName+"NOT THE END????????");
                yield return StartCoroutine(updateDialogueText());
                //yield return interactiveWait;
                //If Internal exists Pause then update internal
                
                if(internalSelfModeB){
                  Debug.LogError(dName+"INTERNAL SELF");
                  //INTERNAL SELF METHOD
                  buttonPressMethod();
                  yield return StartCoroutine(internalSelfMethod());
                  }else{
                    buttonPressMethod();
                    Debug.LogError(dName+"UPDATE BUTTON METHOD");
                    yield return StartCoroutine(readyForButtonPress());
                  }
                }else{
                  //ENDIGN
                  //DO THE END STUFF
                  yield return StartCoroutine(endingMethod());
                }
              }
              //SET THE PRIOR DIALOGUE TO THE NOW
              previousDialogue=nowDialogue;
          }

          yield return tick;
        }
        Debug.Log(dName+"updateScene Done");
    }

  /*
  *
  */
    private void internalSelfMode(string innerSelf, string innerSelfDialogue, float textDelay){
        Debug.Log("Setting innerSelfMode for"+innerSelf);
    }

  /*
  *
  */
    private int getSpeaker(){
        switch (loadDialogue.innerSpeaker[nowDialogue])
        {
            case "red":
                return 0;
            case "green":
                return 1;
            case "blue":
                return 2;
            case "yellow":
                return 3;
            default:
                return 9;
        }
    }
    
  /*
  *
  */
    private void triggerUpdate(int button){  
        needsUpdated=true;
        asheTextNeedsUpdated=true;
        buttonPressed=button;
    }

  /*
  *
  */
    IEnumerator waitForInput(){
        while(!input.UI.Click.triggered){
            yield return null;
        }
        yield return new WaitForSecondsRealtime(0.5f);
        needsUpdated=false;
    }

  /*
  *
  */
    public void OnButtonClick(int buttonIndex){
        if(!buttons.getInteractable(buttonIndex)){
            Debug.Log(dName+"NOPE, MY BUTTON");
        }else{
            Debug.Log(dName+"BUTTON CLICKED"+buttonIndex);
            mainAudioSource.Play();
            triggerUpdate(buttonIndex);
        }
    }

  /*
  *
  */
  private void buttonPressMethod(){
    switch (buttonPressed)
    {
        case 0:
          Debug.LogError(dName+"BUTTON PRESS METHOD BUTTON ONE");
          nowDialogue=loadDialogue.optionLink1[previousDialogue];
          checkEnding();
          break;
        case 1:
          Debug.Log(dName+"BUTTON PRESS METHOD BUTTON TWO");
          nowDialogue=loadDialogue.optionLink2[previousDialogue];
          checkEnding();
          break;
        default:
          break;
    }             
  }

  private void checkEnding(){
    //check if 999
    if(loadDialogue.optionLink1[nowDialogue]==999){
      Debug.LogError(dName+"BUTTON ONE IS 999");
      pendingRaniText=loadDialogue.RaniDialogueText[nowDialogue];
      pendingAsheText=loadDialogue.AsheDialogueText[nowDialogue];
      hardEndGame=true;
      buttonBothHide=true;
    }
    //check if 888
    if(loadDialogue.optionLink1[nowDialogue]==888){
      Debug.LogError(dName+"BUTTON ONE IS 888");
      pendingRaniText=loadDialogue.RaniDialogueText[nowDialogue];
      pendingAsheText=loadDialogue.AsheDialogueText[nowDialogue];
      softEndGame=true;
      buttonBothHide=true;
    }
    //set dialogue if both are true
    if((loadDialogue.optionLink1[nowDialogue]!=999)||(loadDialogue.optionLink1[nowDialogue]!=888)){
      Debug.LogError(dName+"BUTTON ONE IS NOT 999 OR 888");
      //check if 999
      if(loadDialogue.optionLink2[nowDialogue]==999){
        Debug.LogError(dName+"BUTTON ONE IS NOT 999 OR 888 AND BUTTON TWO IS 999");
        pendingAsheText=loadDialogue.AsheDialogueText[nowDialogue];
        pendingRaniText=loadDialogue.RaniDialogueText[nowDialogue];
        buttonTwoHide=true;
      }else if(loadDialogue.optionLink2[nowDialogue]==888){
        Debug.LogError(dName+"BUTTON ONE IS NOT 999 OR 888 AND BUTTON TWO IS 888");
        pendingAsheText=loadDialogue.AsheDialogueText[nowDialogue];
        pendingRaniText=loadDialogue.RaniDialogueText[nowDialogue];
        buttonTwoHide=true;
      }else{
        Debug.LogError(dName+"BUTTON ONE IS NOT 999 OR 888 AND BUTTON TWO IS NOT 999 OR 888");
        pendingRaniText=loadDialogue.RaniDialogueText[nowDialogue];
        pendingAsheText=loadDialogue.AsheDialogueText[nowDialogue];
        buttonBothHide=false;
        buttonTwoHide=false;
     }
    }
  }

  /*
  *
  */
    IEnumerator updateButtons(){
      buttons.setInteractable(false,0);
      if(buttonTwoHide){
        Debug.LogError(dName+"BUTTON TWO HIDE TRUE");
        thinkBubble.setVisibility(true);
        yield return StartCoroutine(buttons.setButtonVisibility(true,1,buttonDelay,buttonTextDelay,loadDialogue.optionText1[nowDialogue],""));
        yield return new WaitForSecondsRealtime(2f);
        buttons.setInteractable(true,1);
      }else
      if(buttonBothHide){
        Debug.LogError(dName+"BUTTON BOTH HIDE TRUE");
        yield return StartCoroutine(buttons.setButtonVisibility(false,0,0f,0f,"",""));
        yield return new WaitForEndOfFrame();
        buttons.setInteractable(false,2);
        buttons.setInteractable(false,1);
      }else
      {
        Debug.LogError(dName+"BUTTON TWO HIDE FALSE");
        Debug.Log(dName+"SET BUTTONS VIS TO: "+true);
        thinkBubble.setVisibility(true);
        yield return StartCoroutine(buttons.setButtonVisibility(true,0,buttonDelay,buttonTextDelay,loadDialogue.optionText1[nowDialogue],loadDialogue.optionText2[nowDialogue]));
        Debug.Log(dName+"SET BUTTONS INTERACTABLE");
        yield return new WaitForSecondsRealtime(2f);
        buttons.setInteractable(true,0);
        
      }
      yield return null;
    }

  /*
  *
  */
    IEnumerator makeInternalVisible(){
      Debug.Log(dName+"set internal self image visible");
      StartCoroutine(internalSelfImage.setVisible(buttonDelay));
      Debug.Log(dName+"set internal self textbox visible");
      StartCoroutine(InternalSelfDialogue.setVisible(internalSelfTransitionDelay[internalSpeaker]));
      yield return null;
    }


  /*
  *
  */
    private IEnumerator firstPassMethod(){
      //reset the now dialogue in case it gets messed up lol, only for the first pass
      nowDialogue=0;
      Debug.Log(dName+"FIRST PASS SET RANI TEXT TO"+loadDialogue.RaniDialogueText[nowDialogue]);
      yield return StartCoroutine(RaniDialogue.setText(loadDialogue.RaniDialogueText[nowDialogue],raniTextDelay));
      yield return StartCoroutine(updateButtons());
      Debug.Log(dName+"FIRST PASS Show THINKING");
      ///thinkBubble.setVisibility(true);
      Debug.Log(dName+"END FIRST PASS");
      firstPass=false;
      needsUpdated=false;
    }

  /*
  *
  */
  private IEnumerator internalSelfMethod(){
    Debug.Log(dName+"inner Dialogue Exists!");
    internalSpeaker=getSpeaker();
    buttons.setInteractable(false,0);
    //wait for input TODO:ADD VISUAL INDICATOR
    yield return StartCoroutine(waitForInput());
    //update Internal
    Debug.Log(dName+"set internal dialogue");
    //update the background image
    Debug.Log(dName+"darken the background");
    darken.setVisibility(true,0.01f);
                   
    //Update the speaker image
    Debug.Log(dName+"set speaker image to "+internalSpeaker);
    internalSelfImage.setImage(internalSpeaker);
    //Make the speaker image visible
    yield return StartCoroutine(makeInternalVisible());
    //set internal dialogue text
    Debug.Log(dName+"set internal self text visible");
    yield return StartCoroutine(InternalSelfDialogue.setText(loadDialogue.innerDialogue[nowDialogue],internalSelfTextDelay[internalSpeaker]));
                  
    yield return StartCoroutine(waitForInput());

    Debug.Log(dName+"set think bubble vis");
    
    //update Buttons
    Debug.Log(dName+"UPDATE BUTTONS");
    yield return StartCoroutine(updateButtons());
  }



  /*
  *
  */
  private void hideTheThings(){
    Debug.Log(dName+"NEEDS UPDATED");
    //HIDE ALL THE THINGS HERE
    Debug.Log(dName+"SET BUTTON NOT INTERACTIVE");
    buttons.setInteractable(false,0);
    StartCoroutine(buttons.setButtonVisibility(false,0,0f,buttonTextDelay,"",""));
    thinkBubble.setVisibility(false);
    darken.setVisibility(false,0f);
    StartCoroutine(internalSelfImage.setInvisible(0f));
    StartCoroutine(InternalSelfDialogue.setInvisible(0f));
    InternalSelfDialogue.removeText();
  }

  /*
  *
  */
  private IEnumerator updateDialogueText(){
    Debug.Log(dName+"set Ashe Dialogue to"+pendingAsheText);
    //update Ashe
    yield return StartCoroutine(AsheDialogue.setText(pendingAsheText,asheTextDelay));
    Debug.Log(dName+"set Rani Dialogue to"+pendingRaniText);
    //update Rani
    yield return StartCoroutine(RaniDialogue.setText(pendingRaniText,raniTextDelay));
  }

  /*
  *
  */
  private IEnumerator endingMethod(){
    if(hardEndGame){
    thinkBubble.setVisibility(false);
    Debug.Log(dName+"set Ashe Dialogue to"+pendingAsheText);
    //update Ashe
    yield return StartCoroutine(AsheDialogue.setText(pendingAsheText,asheTextDelay));
    Debug.Log(dName+"set Rani Dialogue to"+pendingRaniText);
    //update Rani
    yield return StartCoroutine(RaniDialogue.setText(pendingRaniText,raniTextDelay));
    
    targetEndDialogue=loadDialogue.optionText1[nowDialogue];
    targetEnding=1;

    Debug.Log(dName+"END GAME HARD");
    transScene.button();
    }
    if(softEndGame){
      thinkBubble.setVisibility(false);
                    
      //previousDialogue=0;
      //nowDialogue=0;
      Debug.Log(dName+"set Ashe Dialogue to"+pendingAsheText);
      //update Ashe
      yield return StartCoroutine(AsheDialogue.setText(pendingAsheText,asheTextDelay));
      Debug.Log(dName+"set Rani Dialogue to"+pendingRaniText);
      //update Rani
      yield return StartCoroutine(RaniDialogue.setText(pendingRaniText,raniTextDelay));
      targetEndDialogue=loadDialogue.optionText1[nowDialogue];
      targetEnding=0;
      Debug.Log(dName+"END GAME SOFT");
      transScene.button();
                    
      //Display ending blurb

    }
  }

  /*
  *
  */
  private IEnumerator readyForButtonPress(){
    yield return StartCoroutine(updateButtons());
  }

  /*
  *
  */
  private void checkForPotatos(){
    if(!(loadDialogue.innerSpeaker[nowDialogue]=="potato")){
      internalSelfModeB=true;
    }else{
      internalSelfModeB=false;
    }
  }

  /*
  *
  */
  private void cycleEnd(){
    
  }
}