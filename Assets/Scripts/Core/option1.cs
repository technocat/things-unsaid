﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Bolt;
public class option1 : MonoBehaviour
{
    // Start is called before the first frame update
public TMP_Text option1Text;
    private List<string> text;
    private int currentEntry = 0;
    private bool loaded=false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentEntry = (int)Variables.Application.Get("currentEntry");
        loadIfNotLoaded();
        updateIfNew();
    }

    //todo make smert
    void loadIfNotLoaded(){
        if (!loaded){
            text = (List<string>)Variables.Application.Get("option1Text");
            loaded=true;
        }
    }

    void updateIfNew(){
        if (option1Text.text != text[currentEntry]){
            option1Text.text = text[currentEntry];
        }
    }
}
