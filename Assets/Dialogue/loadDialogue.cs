﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using Ludiq;
using Bolt;


public class loadDialogue : MonoBehaviour
{
    string dialoguePath = "Assets/Dialogue/dialogueFormat.txt";

    int currentEntry;
    private List<int> entryID = new List<int>{};//: 1
    private List<string> speaker=new List<string>{};//: "Friend"
    private List<int> backgroundImage=new List<int>{};//:0
    private List<string> dialogueText=new List<string>{};//:"Hello~"
    private List<string> optionText1=new List<string>{};//:"uhhhhh"
    private List<string> optionText2=new List<string>{};//:"I Have No Clue what to Say"
    private List<int> optionLink1=new List<int>{};//:1
    private List<int> optionLink2=new List<int>{};//:2

    

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            //Create instance of StreamReader to read from file, also close it
            using (StreamReader sr = new StreamReader(dialoguePath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    //Debug.Log("\n");
                    //Debug.Log(line);
                    
                    string[] temp = line.Split(':');
                    
                    //Debug.Log(temp[0]);
                    //Debug.Log(temp[1]);
                    
                    lineParser(temp);
                    /*
                    foreach (var entry in temp)
                    {
                        Debug.Log("This is the Entry:");
                        Debug.Log(entry);
                    }
                    */
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError("File could not be read:");
            Debug.LogError(e.Message);
        }

        //set the Application List Variables to the internal List variables
        Variables.Application.Set("entryID",entryID);
        Variables.Application.Set("speaker",speaker);
        Variables.Application.Set("backgroundImage",backgroundImage);
        Variables.Application.Set("dialogueText",dialogueText);
        Variables.Application.Set("option1Text",optionText1);
        Variables.Application.Set("option2Text",optionText2);
        Variables.Application.Set("option1Link",optionLink1);
        Variables.Application.Set("option2Link",optionLink2);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Iterate through the String Array line by line, adding that information into an dialogueHolder
    private void lineParser(string[] sA){
        //Debug.Log(sA[0]);
        //Debug.Log(sA[1]);
        switch (sA[0]){
            case "entry"://: 1
                entryID.Add(stringToInt(sA[1]));
                Debug.Log("Entry:"+sA[0]+"-"+sA[1]);
                break;
            case "who"://: "Friend"
                speaker.Add(sA[1]);
                Debug.Log("speaker:"+sA[0]+"-"+sA[1]);
                break;
            case "background"://:0
                backgroundImage.Add(stringToInt(sA[1]));
                Debug.Log("background:"+sA[0]+"-"+sA[1]);
                break;
            case "dialogue"://:"Hello~"
                dialogueText.Add(sA[1]);
                Debug.Log("dialogueText:"+sA[0]+"-"+sA[1]);
                break;
            case "optionText_1"://:"uhhhhh"
                optionText1.Add(sA[1]);
                Debug.Log("OptionText1:"+sA[0]+"-"+sA[1]);
                break;
            case "optionText_2"://:"I Have No Clue what to Say"
                optionText2.Add(sA[1]);
                Debug.Log("OptionText2:"+sA[0]+"-"+sA[1]);
                break;
            case "optionLink_1"://:1
                optionLink1.Add(stringToInt(sA[1]));
                Debug.Log("optionLink1:"+sA[0]+"-"+sA[1]);
                break;
            case "optionLink_2"://:2
                optionLink2.Add(stringToInt(sA[1]));
                Debug.Log("OptionLink2:"+sA[0]+"-"+sA[1]);
                break;
            default:
                Debug.LogError("INVALID TEXT ENTRY");
                Debug.LogError(sA[0]+"-"+sA[1]);
                break;
        }
            
        
        

    }

    private int stringToInt(string s){
        int i = 0;
        try{
            i = Int16.Parse(s);
        } catch (Exception e){ Debug.LogError(e);}
        return i;
    }

    public int getEntryID(int id){
        return entryID[id];
    }
    public string getSpeaker(int id){
        return speaker[id];
    }

    public int getBackgroundImage(int id){
        return backgroundImage[id];
    }
    public string getDialogueText(int id){
        return dialogueText[id];
    }
    public string getOptionText1(int id){
        return optionText1[id];
    }
    public string getOptionText2(int id){
        return optionText2[id];
    }
    public int getOptionLink1(int id){
        return optionLink1[id];
    }
    public int getOptionLink2(int id){
        return optionLink2[id];
    }


}
