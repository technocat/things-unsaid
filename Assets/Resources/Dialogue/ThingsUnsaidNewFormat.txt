entry:0
who:Rani
background:0
RaniDialogue:Oh… hey!
AsheDialogue:
optionText_1:Oh gosh what was her name? Rayna? Ramya? Rani!
optionLink_1:1
optionText_2:Oh my gosh it's Rani!!
optionLink_2:2
innerSpeaker:potato
innerDialogue:potato
entry:1
who:Rani
background:0
RaniDialogue:So… uh, it's been a long time hasn't it… almost nine years.
AsheDialogue:Oh… hi… Rani.
optionText_1:Why is this so awkward?
optionLink_1:3
optionText_2:I didn't even realise.
optionLink_2:4
innerSpeaker:red
innerDialogue:It really HAS been almost nine years. Wow. This is so awkward, how are you supposed to respond to that??? What do we even talk about after NINE years??
entry:2
who:Rani
background:0
RaniDialogue:Hey! Yeah it's been a long time. Almost nine years.
AsheDialogue:Rani! It's been a while!
optionText_1:It hasn't been that long has it?
optionLink_1:5
optionText_2:Everything seemed like it was yesterday.
optionLink_2:6
innerSpeaker:green
innerDialogue:She hates you for it. You used to be so close…nine years can pass by so quickly. It felt just like yesterday we were all hanging out in school.
entry:3
who:Rani
background:0
RaniDialogue:So… how have you been? What are you up to?
AsheDialogue:Uh, yeah haha.
optionText_1:THIS IS AN SOFT END
optionLink_1:888
optionText_2:Life is… alright.
optionLink_2:888
innerSpeaker:blue
innerDialogue:Life has been good but it's still tough sometimes. You have work piling up and you're not even the best at what you do still, but I guess you work hard, and get the job done most of the time.
entry:4
who:Rani
background:0
RaniDialogue:Isn't it? How are you? What have you been up to?
AsheDialogue:Yeah, it's crazy how fast time passes.
optionText_1:THIS IS A HARD AF END
optionLink_1:999
optionText_2:Life is… alright.
optionLink_2:999
innerSpeaker:blue
innerDialogue:Life has been good but it's still tough sometimes. You have work piling up and you're not even the best at what you do still, but I guess you work hard, and get the job done most of the time.
entry:5
who:Rani
background:0
RaniDialogue:Um. Well. Yeah. So what have you been up to?
AsheDialogue:Has it been THAT long?
optionText_1:THIS GOES TO ONE
optionLink_1:1
optionText_2:THIS SHOULD BE HIDDEN
optionLink_2:999
innerSpeaker:blue
innerDialogue:Life has been good but it's still tough sometimes. You have work piling up and you're not even the best at what you do still, but I guess you work hard, and get the job done most of the time.
entry:6
who:Rani
background:0
RaniDialogue:Doesn't it? How have you been? We should catch up.
AsheDialogue:Wow… it's crazy how high school doesn't seem that long ago.
optionText_1:THIS IS A SOFT END
optionLink_1:888
optionText_2:Life is… alright.
optionLink_2:888
innerSpeaker:blue
innerDialogue:Life has been good but it's still tough sometimes. You have work piling up and you're not even the best at what you do still, but I guess you work hard, and get the job done most of the time.