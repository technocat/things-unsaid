A visual novel style video game, focused on the story of two friends who are reconnecting after some time being apart.

Final project for our game theory course in university.

Made in Unity on Linux and published to the class using WebGL.

Playable at https://madds.hollandart.io/things_unsaid

To see the main parts of the code, peruse the Assets/Scripts folder, ignoring the .meta files.

The Core folder there holds the bulk of the logic, Dialoge holds the Dialoge Boxes and the loading parser, Macros is Unity Bolt scripts for the UI, and Image has the scripts for loading the images for the objects.

This is the state of the game when submitted for class, and it's the version which lives on my website. The game was solo-coded by me, so VCS wasn't much of a consideration. However, it would have saved me some headache when Unity broke a few times so I'm commited to having VCS for future projects.

Story by Rahmah

Visuals by Sarah

Audio by Nik

UI and Programming by Madds (me)

#Copyright
---
The code (in Assets/Scripts) is CC 4.0 by Atttribution
https://creativecommons.org/licenses/by/4.0/legalcode

The story (everything inside Assets/Resources/Dialogue/) is Copyright Rahmah, here for example and not free to use.

The images (in Assets/Resources/Art/Characters) are copyright Sarah, here for example and also not free to use.

The audio (Assets/Resources/Audio) is copyright Nik and not free to use.

Other assets are either copyright Madds or have their copyright belonging to their respective owners, used by permission either by lisence or as it's CC.
